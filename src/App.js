// src/App.js
/*
import React from 'react';
import './App.css';
import Login from './Components/login';
import Dashboard from './Components/dashboard';
import Admin from './Components/admin';

function App() {
  return (
    <div className="App">
      <Dashboard />
    </div>
  );
}

export default App;
*/

import React from 'react';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import Login from './Components/login';
import Dashboard from './Components/dashboard';
import Admin from './Components/admin';
import MakeAccount from './Components/makeAccount';

const App = () => {
  return (
          <Routes>
              	
              <Route path="/make-account" element={<MakeAccount />} />
              <Route path="/admin" element={<Admin />} />
              <Route path="/" element={<Dashboard />} />
              <Route path="/login" element={<Login />} />
              
          </Routes>
  );
};
export default App;