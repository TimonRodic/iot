import React, { useState, useEffect } from 'react';
import './admin.css';
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';

const Admin = () => {
    const [imageSrc, setImageSrc] = useState('');
    const [xSigns, setXSigns] = useState([]);
    const [requiredXSigns, setRequiredXSigns] = useState(10); // Initial state set to 0 TO DO : jel oke da to ostane hardocirano, da korsinik stavlja dva senzora
    const [currentId, setCurrentId] = useState(1);
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [floorPlans, setFloorPlans] = useState([]);
    const [xSignIds, setXSignIds] = useState([]);
    const [users, setUsers] = useState([]);
    const [file, setFile] = useState('');

    let [roomId, setRoomId] = useState(0); // kad se spremi plan vrati api
    let [assetId, setAssetId] = useState(0);  // kad se spremi plan vrati api
    let [roomName, setRoomName] = useState(''); //daje korisnik

    const navigate = useNavigate();

    const roomIdFunction = (p) => {
        setRoomId(roomId = p)
      }

    const assetIdFunction = (p) => {
        setAssetId(assetId = p)
    }

    const roomNameFunction = (p) => {
        setRoomName(roomName = p)
    }

    useEffect(() => {
        // Fetch floor plans and X sign information from API
        const fetchData = async () => {
            try {
                // Fetching users - DONE
                const userResponse = await fetch('https://iot-1-uo7o.onrender.com/api/v1/users');
                const usersData = await userResponse.json();
                setUsers(usersData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        setFile(file)
        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                setImageSrc(e.target.result);
                setXSigns([]);
                setCurrentId(xSignIds[0]);
                setSelectedUsers([]);
            };
            reader.readAsDataURL(file);
        }
    };

    const handleImageClick = (event) => {
        if (xSigns.length < requiredXSigns) {
            const img = document.getElementById('uploaded-image');
            const rect = img.getBoundingClientRect();
            const x = event.clientX - rect.left;
            const y = event.clientY - rect.top;
            const xPercent = (x / rect.width) * 100;
            const yPercent = (y / rect.height) * 100;
    
            const deviceName = window.prompt("Enter device name:");
            console.log(deviceName)
            const deviceLabel = window.prompt("Enter device label:");
    
            if (deviceName && deviceLabel) {
                setXSigns([...xSigns, { id: currentId, x: xPercent, y: yPercent, deviceName: deviceName, deviceLabel: deviceLabel }]);
                const nextIdIndex = xSigns.length + 1;
                if (nextIdIndex < xSignIds.length) {
                    setCurrentId(xSignIds[nextIdIndex]);
                }
            }
        }
    };

    const handleUserCheckboxChange = (event) => {
        const user = parseInt(event.target.id);
        if (event.target.checked) {
            setSelectedUsers([...selectedUsers, user]);
        } else {
            setSelectedUsers(selectedUsers.filter(selectedUser => selectedUser !== user));
        }
    };

    const handleSaveXSigns = async () => {
         const myFile = document.querySelector("input[type=file]").files[0];
         const formdata = new FormData()
         formdata.append('file', myFile); 
         formdata.append('name', roomName)

         try {
             const response = await fetch('https://iot-1-uo7o.onrender.com/api/v1/floor-plans/upload', {
                 method: 'POST',
                 body: formdata,
             });
 
             if (!response.ok) {
                toast.error('something went wrong please try again', {
                    position: "top-right",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    });
                 throw new Error('Network response was not ok');
             }
 
             const responseData = await response.text();
             console.log('Response:', responseData);
             const regex = /(\d+)$/;
             const match = responseData.match(regex);

            const id = match[1];
            console.log(`Extracted ID: ${id}`);

            const roomResponse = await fetch('https://iot-1-uo7o.onrender.com/api/v1/floor-plans');
            const rommData = await roomResponse.json();
            console.log(rommData)
            const item = rommData.find(item => item.id == id);
            console.log(item)
            const item_assetId = item['assetId']
            assetIdFunction(item_assetId)
            roomIdFunction(id)

            xSigns.forEach(senzor => {
                const outputData = {
                    deviceName: senzor.deviceName,
                    deviceLabel: senzor.deviceLabel,
                    point: {
                        x: senzor.x,
                        y: senzor.y
                    }
                };
                console.log(outputData);
                fetch("https://iot-1-uo7o.onrender.com/api/v1/thingsboard/devices/" + item_assetId, {
                    method: "POST",
                    headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify(outputData),
                  }).then((res) => {
                    if(res.status == 200){
                        toast.success('Cordinates and floor plan saved', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            theme: "colored",
                            });
                        console.log("saved")
                    } else{
                        toast.error('Something went wrong please try again', {
                            position: "top-right",
                            autoClose: 2000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            theme: "colored",
                            });
                    }
                  })
            });

         } catch (error) {
             console.error('Error', error);
         }
        console.log('Coordinates:', xSigns);
    };

    const handleSaveUsers = async () => {
        console.log('Selected Users:', selectedUsers);

        let url = 'https://iot-1-uo7o.onrender.com/api/v1/floor-plans/' + roomId
        try {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(selectedUsers),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const responseData = await response.json();
            console.log('Response:', responseData);
            toast.success('Users successfully added', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
        } catch (error) {
            console.error('Error saving selected users:', error);

            toast.error('Something went wrong, please try again', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
        }
    };

    return (
        <div className="container">
            <div><FontAwesomeIcon icon={faHome}  onClick={() => navigate('/')}/><h1>The Sound of Silence</h1></div>
            <ToastContainer/>
            <div>
                    

                <label>
                    Room Name:
                    <input 
                        onChange={(e) => roomNameFunction(e.target.value)}
                        type="text" placeholder="room_name" />
                </label>
                <button onClick={() => document.getElementById('file-input').click()}>Upload</button>
                <input 
                    type="file" 
                    id="file-input" 
                    accept="image/*" 
                    style={{ display: 'none' }} 
                    onChange={handleFileChange} 
                />
            </div>
            {imageSrc && xSigns.length < requiredXSigns && (
                <p>Put an X for location of a sensor</p>
            )}
            <div id="image-container" onClick={handleImageClick}>
                {imageSrc && <img id="uploaded-image" src={imageSrc} alt="Uploaded Ground Plan" />}
                {xSigns.map((sign) => (
                    <div
                        key={sign.id}
                        className="x-sign"
                        style={{
                            left: `${sign.x}%`,
                            top: `${sign.y}%`
                        }}
                    >
                        X
                    </div>
                ))}
            </div>

            <div>
                <button onClick={handleSaveXSigns}>Save X Signs</button>
            </div>

            <div>
                <h2>Select Users</h2>
                {users.map(user => (
                    <div key={user.id}>
                        <input
                            type="checkbox"
                            id={`${user.id}`}
                            value={user.email}
                            onChange={handleUserCheckboxChange}
                        />
                        <label htmlFor={`user-${user.id}`}>{user.email}</label>
                    </div>
                ))}
            </div>

            {selectedUsers.length > 0 && (
                <div>
                    <button onClick={handleSaveUsers}>Save Users</button>
                </div>
            )}
        </div>
    );
};

export default Admin;
