import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import './login.css';

const MakeAccount = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');

  const handleEmailChange = (e) => setEmail(e.target.value);
  const handlePasswordChange = (e) => setPassword(e.target.value);
  const handleNameChange = (e) => setName(e.target.value);
  const handleLastNameChange = (e) => setLastName(e.target.value);

  const login = async (e) => {
    e.preventDefault();
    console.log('login!');
    try {
      const response = await fetch('https://iot-1-uo7o.onrender.com/api/v1/users', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: email,
          firstName: name,
          lastName: lastName,
          password: password,

        }),
      });
      const status = await response.status;
      if (status == 201) {
        localStorage.clear()
        navigate('/');
      } else {
        alert('something went wrong, please try again!');
      }
    } catch (error) {
      console.error('Login error:', error);
    }
  };

  return (
    <div className="container">
      <h1>The Sound of Silence</h1>
      <p>Welcome, please fill in the data to make an account</p>
      <form>
      <label>
          first name
          <input
            type="text"
            placeholder="name"
            value={name}
            onChange={handleNameChange}
          />
        </label>
        <label>
          last name
          <input
            type="text"
            placeholder="last_name"
            value={lastName}
            onChange={handleLastNameChange}
          />
        </label>
        <label>
          e-mail
          <input
            type="text"
            placeholder="email"
            value={email}
            onChange={handleEmailChange}
          />
        </label>
        <label>
          password
          <input
            type="password"
            placeholder="password"
            value={password}
            onChange={handlePasswordChange}
          />
        </label>
        <button type="submit" onClick={login}>
          submit
        </button>
      </form>
      <div>Already have an account, log in: <button onClick={() => navigate('/login')}>Login</button></div>
    </div>
  );
};

export default MakeAccount;
