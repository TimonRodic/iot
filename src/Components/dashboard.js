import React, { useRef, useEffect, useState } from 'react';
import './dashboard.css';
import { useLocation, useNavigate } from "react-router";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBullseye } from '@fortawesome/free-solid-svg-icons';

const Dashboard = () => {
  const [floorPlans, setFloorPlans] = useState([]);
  let [selectedFloorPlan, setSelectedFloorPlan] = useState(null);
  let [selectedAssetId, setSelectedAssetId] = useState(null);
  const [imageLoaded, setImageLoaded] = useState(false);
  const [warningMessage, setWarningMessage] = useState('');
  let [coordinates, setCoordinates] = useState([]);
  const imageRef = useRef(null);

  const navigate = useNavigate();
  const user = localStorage.getItem("user");
  const role = localStorage.getItem("role");

  const [data, setData] = useState(null);
  const POLLING_INTERVAL = 3000; // Poll every 3 seconds

  const selectedFloorPlanFunction = (p) => {
    setSelectedFloorPlan(selectedFloorPlan = p);
  };

  const cordinatesFunction = (p) => {
    setCoordinates(coordinates = p);
  };

  const selectedAssetIdFunction = (p) => {
    setSelectedAssetId(selectedAssetId = p);
  };

  useEffect(() => {
    const intervalId = setInterval(() => {
      fetchRealTimeData();
    }, POLLING_INTERVAL);

    async function fetchVolume(deviceId) {
      const response = await fetch(`https://iot-1-uo7o.onrender.com/api/v1/thingsboard/telemetry/${deviceId}`);
      const data = await response.json();
      return data.volume[0].value == null ? 0 : data.volume[0].value;
    }

    async function fetchRealTimeData() {
      setWarningMessage('');
      const d = new Date();
      document.getElementById("date").innerHTML = d;

      const volumeDataPromises = coordinates.map(async coord => {
        const volume = await fetchVolume(coord.id);
        return {
          id: coord.id,
          x: coord.x,
          y: coord.y,
          volume: parseFloat(volume) // Convert the volume to a float
        };
      });

      const volumeData = await Promise.all(volumeDataPromises);
      updateSymbolColors(volumeData);
    }

    fetchRealTimeData(); // Initial fetch

    return () => clearInterval(intervalId); // Cleanup on component unmount
  }, [coordinates]);

  const updateSymbolColors = (volumeData) => {
    let highVolumeCount = 0;
    coordinates.forEach(cord => {
      const symbol = document.querySelector('.symbol-x' + cord.id);
      const volume = volumeData.find(data => data.id == cord.id)?.volume;
      if (volume !== undefined) {
        symbol.classList.remove('glow-low', "glow-medium", "glow-high");
        if (volume == 0) {
          symbol.style.color = 'black';
        } else if (volume < 250) {
          symbol.classList.add('glow-low');
        } else if (volume >= 250 && volume < 500) {
          symbol.classList.add('glow-medium');
        } else {
          symbol.classList.add("glow-high");
          highVolumeCount += 1;
        }
      }
    });
    if (highVolumeCount > 0) {
      toast.error('Warning: High volume detected', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      });
    }
  };

  useEffect(() => {
    if (!localStorage.getItem("user")) {
      navigate("/login");
    }
    const fetchData = async () => {
      try {
        const response = await fetch('https://iot-1-uo7o.onrender.com/api/v1/users/' + localStorage.getItem("user"), {
          method: 'GET',
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        const responseData = await response.json();
        setFloorPlans(responseData['floorPlans']);
      } catch (error) {
        console.error('Error fetching floor plans:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (selectedFloorPlan) {
      fetchFloorPlanData(selectedFloorPlan.id);
    }
  }, [selectedFloorPlan]);

  const fetchFloorPlanData = async (floorPlanId) => {
    try {
      const response = await fetch('https://iot-1-uo7o.onrender.com/api/v1/floor-plans/' + selectedFloorPlan, {
        method: 'GET',
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const binaryData = await response.arrayBuffer();
      const blob = new Blob([binaryData], { type: 'image/png' });
      const url = URL.createObjectURL(blob);

      const imgElement = document.getElementById('image');
      imgElement.src = url;

      const coordResponse = await fetch('https://iot-1-uo7o.onrender.com/api/v1/thingsboard/devices/' + selectedAssetId, {
        method: 'GET',
      });

      if (!coordResponse.ok) {
        throw new Error('Network response was not ok');
      }

      const responseData = await coordResponse.json();
      const result = responseData.map(item => {
        const [x, y] = item.additionalInfo.description.split(',').map(Number);
        return {
          x,
          y,
          id: item.id.id,
          label: item.label,
          name: item.name
        };
      });

      cordinatesFunction(result);
    } catch (error) {
      console.error('Error fetching floor plan data:', error);
    }
  };

  const handleFloorPlanChange = (event) => {
    const idRegex = /id:(\d+)/;
    const assetIdRegex = /asset_id:(\S+)/;

    const idMatch = event.target.value.match(idRegex);
    const assetIdMatch = event.target.value.match(assetIdRegex);

    if (idMatch && assetIdMatch) {
      const id = idMatch[1];
      const assetId = assetIdMatch[1];
      selectedFloorPlanFunction(id);
      selectedAssetIdFunction(assetId);
    } else {
      console.log("No matches found");
    }
  };

  const getSymbolPosition = (coord) => ({
    left: `${coord.x}%`,
    top: `${coord.y}%`
  });

  const getSymbolPositionLabel = (coord) => ({
    left: `${coord.x}%`,
    top: `${coord.y + 5}%`
  });

  const logout = () => {
    localStorage.clear();
    navigate("/login");
  };

  const deleteSelectedFloorPlan = async () => {
    console.log(selectedFloorPlan)
    
    if (!selectedFloorPlan) {
      toast.error('No floor plan selected for deletion.');
      return;
    }

    try {
      const response = await fetch(`https://iot-1-uo7o.onrender.com/api/v1/floor-plans/${selectedFloorPlan}`, {
        method: 'DELETE',
      });

      if (!response.ok) {
        throw new Error('Failed to delete floor plan');
      }

      toast.success('Floor plan deleted successfully.');
      window.location.reload()
    } catch (error) {
      toast.error('Error deleting floor plan: ' + error.message);
    }
    
  };

  if (!user) {
    navigate("/login");
  }

  return (
    <div className="dashboard-container">
      <header>
        <h1>The Sound of Silence</h1>
        <ToastContainer />
        <nav>
          <ul>
            <li><button className="logout-button" onClick={logout}>LOG OUT</button></li>
            {user && role === 'ROLE_ADMIN' && (
              <li><button className="add-floorplan-button" onClick={() => navigate('/admin')}>ADD FLOOR PLAN</button></li>
            )}
          </ul>
        </nav>
      </header>
      <main>
        <div className="controls">
          <label htmlFor="floor-select">Choose a ground plan to preview sound heat map:</label>
          <select id="floor-select" onChange={handleFloorPlanChange}>
            <option value="" selected disabled>Select floor plan</option>
            {floorPlans.map(plan => (
              <option key={plan.id} value={`id:${plan.id}asset_id:${plan.assetId}`}>{plan.name}</option>
            ))}
          </select>
        </div>
        <div className="warning-message" style={{ color: "red" }}>{warningMessage}</div>
        <div className="map-container">
          <img
            id='image'
            src=""
            alt="floor plan"
            className="heatmap"
            ref={imageRef}
            onLoad={() => setImageLoaded(true)}
          />
          {imageLoaded && coordinates.map((coord, index) => (
            <div
              key={coord.id}
              className={"symbol-x" + coord.id}
              style={getSymbolPosition(coord)}
            >
              <FontAwesomeIcon icon={faBullseye} />
            </div>
          ))}
          {imageLoaded && coordinates.map((coord, index) => (
            <div
              key={coord.id}
              className={"symbol-x" + coord.id}
              style={getSymbolPositionLabel(coord)}
            >
              {coord.name}
            </div>
          ))}
        </div>
      </main>
      <footer>
        <div className="datetime">
          <span id='date'>date</span>
          <span>Zagreb, Croatia</span>
          {imageLoaded && user && role === 'ROLE_ADMIN' && (
            <button className="delete-floorplan-button" onClick={deleteSelectedFloorPlan}>DELETE FLOOR PLAN</button>
          )}
        </div>
      </footer>
    </div>
  );
};

export default Dashboard;
