// src/components/Login.js
import React, { useEffect, useState }  from 'react'
import { useLocation, useNavigate } from "react-router";
import './login.css';
import { password } from 'pg/lib/defaults';



const Login = () => {

  const navigate = useNavigate();

  async function login(e){
    e.preventDefault()
    console.log("login!")
    fetch("https://iot-1-uo7o.onrender.com/api/v1/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    }).then((res) => res.json())
      .then(data => {
        console.log("tu")
        console.log(data)
        if(data.message === 'User authenticated.'){
          localStorage.setItem("profil", data.token);
          localStorage.setItem("user",data.userId)
          localStorage.setItem("role", data.role)
          navigate("/");
    
        } else{
          alert("wrong username and/or password, please try again!");
        }
    });
  }


  let  [email, setMail] = useState("");
  let [password, setPassword] = useState("");
  const usermail = (mail) => {
    setMail(email = mail)
  }

  const userpassword = (pass) => {
    setPassword(password = pass)
  }


  return (
    <div className="container">
      <h1>The Sound of Silence</h1>
      <p>Welcome, please login to proceed</p>
      <form>
        <label>
          e-mail
          <input 
            onChange={(e) => usermail(e.target.value)}
            type="text" placeholder="email@gmail.com" />
        </label>
        <label>
          password
          <input 
            onChange={(e) => userpassword(e.target.value)}
            type="password" placeholder="password" />
        </label>
        <button type="submit" onClick={login}>Login</button>
      </form>
      <div>If you don't have an account, make one: <button onClick={() => navigate('/make-account')}>Make Account</button></div>
    </div>
  );
};

export default Login;